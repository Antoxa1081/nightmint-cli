#!/usr/bin/env node

const path = require("path");
const fs = require("fs");
const ChildProcess = require("child_process");

const PROJECT_ROOT = process.cwd();
const ARGV = process.argv.slice(2);

const REPOSITORY_URL = "https://gitlab.com/TecHoof/nightmint/nightmint-dev";

const template = p => r => `mkdir ${p} && cd ${p} && git clone ${r} .`;

createProject(PROJECT_ROOT, REPOSITORY_URL, ARGV, template);

// createProjectFromArchive(PROJECT_ROOT, REPOSITORY_ARCHIVE_URL, ARGV);

function createProject(projectRoot, repositoryUrl, args, execTemplate) {
    const name = args.length > 0 ? args[0] : "nightmint-project";
    const projectPath = path.join(projectRoot, name);

    if (!fs.existsSync(projectPath)) {
        console.log(`Create a new project:\n${projectPath}\n`);
        console.log("Download git repository..");

        const execCommand = execTemplate(projectPath)(repositoryUrl);

        ChildProcess.exec(execCommand, (error, stdout, stderr) => {
            if (error) throw error;
            removeDir(projectPath + "/.git/");
            console.log(stdout);
            console.log(`Nightmint source code: ${repositoryUrl}`);
        });
    } else {
        console.error(new Error(`Path already in exist`));
    }
}

function removeDir(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file, index) {
            let curPath = path + "/" + file;

            if (fs.lstatSync(curPath).isDirectory()) {
                removeDir(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}
